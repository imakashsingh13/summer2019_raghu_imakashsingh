from rest_framework import serializers
from onlineapp.models import *

class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = College
        fields = ('id','name','location','acronym','contact')

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id','name','dob','email','db_folder','dropped_out','college')

class MockSerializer(serializers.ModelSerializer):
    class Meta:
        model = MockTest1
        fields = ('problem1','problem2','problem3','problem4','total')

class StudentDetailSerializer(serializers.ModelSerializer):
    mocktest = MockSerializer(many=True,read_only=True)