#define _CRT_SECURE_NO_WARNINGS
#include "external_sort.h"


int main()
{
	FILE *input_file;
	fopen_s(&input_file,"input.txt","r");
	int buffer_size;
	cout << "\nEnter Buffer size : ";
	cin >> buffer_size;
	int *arr = (int *)malloc(buffer_size * sizeof(int));
	int file_size = find_file_size(input_file);
	cout << file_size;
	int no_of_files = break_into_smaller_files(input_file,arr,buffer_size,file_size);
	merge_all_files(input_file, no_of_files);
	system("pause");
	return 0;
}