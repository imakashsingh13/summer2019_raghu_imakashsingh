#define _CRT_SECURE_NO_WARNINGS
#include "external_sort.h"


int find_min_value_index(int *comparision_array,int *non_exhausted_files , int no_of_files,int *min)
{
	*min = 10000001;
	int min_index = 0;
	for (int i = 0; i < no_of_files; i++)
	{
		if ((non_exhausted_files[i] == 0) && (comparision_array[i] < (*min)))
		{
			*min = comparision_array[i];
			min_index = i;
		}
	}
	return min_index;
}

bool all_files_exhausted(int *non_exhausted_files,int no_of_files)
{
	int ssum = 0;
	for (int i = 0; i < no_of_files; i++)
	{
		ssum += non_exhausted_files[i];
	}
	if (ssum == -(no_of_files))
		return true;
	else
		return false;
}

int find_file_size(FILE *input_file)
{
	char dummy[10];
	int count = 0;
	while (fscanf_s(input_file,"%d",dummy)==1)
	{
		count++;
	}
	return count;
}

int break_into_smaller_files(FILE *input_file,int* arr,int buffer_size,int file_size)
{
	rewind(input_file);
	int no_of_files = file_size / buffer_size;

	for (int i = 0; i < no_of_files; i++)
	{
		char buff[15];
		char file_name[25] = "text_";
		strcat(file_name, _itoa(i, buff, 10));
		strcat(file_name, ".txt");
		FILE *fp;
		fopen_s(&fp,file_name, "w");
		for (int j = 0; j < buffer_size; j++)
		{
			fscanf_s(input_file,"%d",&arr[j]);
		}
		sort(arr, arr + buffer_size);
		for (int j = 0; j < buffer_size; j++)
		{
			fprintf_s(fp,"%d\n",arr[j]);
		}
		fclose(fp);
	}
	fclose(input_file);
	return no_of_files;
}

void merge_all_files(FILE *input_file,int no_of_files)
{
	FILE *output_file = fopen("output.txt","w");

	rewind(input_file);
	FILE **arr_of_file_ptrs = (FILE **)malloc(sizeof(FILE *));
	for (int i = 0; i < no_of_files; i++)
		arr_of_file_ptrs[i] = (FILE *)malloc(sizeof(FILE));

	int *comparision_array = (int *)malloc(no_of_files * sizeof(int));

	int *non_exhausted_files = (int *)calloc(no_of_files, sizeof(int));
	for (int i = 0; i < no_of_files; i++)
	{
		char buff[15];
		char file_name[25] = "text_";
		strcat(file_name, _itoa(i, buff, 10));
		strcat(file_name, ".txt");
		arr_of_file_ptrs[i] = fopen(file_name, "r");
	}
	for (int i = 0; i < no_of_files; i++)
	{
		fscanf(arr_of_file_ptrs[i], "%d", &comparision_array[i]);
	}
	while (!all_files_exhausted(non_exhausted_files, no_of_files))
	{
		int return_val = 0;
		int index = find_min_value_index(comparision_array, non_exhausted_files, no_of_files,&return_val);
		
		fprintf(output_file, "%d\n", comparision_array[index]);
		
		if (fscanf(arr_of_file_ptrs[index], "%d", &comparision_array[index]) == EOF)
		{
			non_exhausted_files[index] = -1;
		}
		else
		{
			fscanf(arr_of_file_ptrs[index], "%d", &comparision_array[index]);
		}
	}
	for (int i = 0; i < no_of_files; i++)
	{
		fclose(arr_of_file_ptrs[i]);
	}
	fclose(output_file);
}