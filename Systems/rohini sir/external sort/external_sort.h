#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <algorithm>

using namespace std;

int find_file_size(FILE *input_file);

int break_into_smaller_files(FILE *input_file, int* arr,int buffer_size,int file_size);

void merge_all_files(FILE * output_file,int no_of_files);

int find_min_value_index(int *comparision_array, int *non_exhausted_files, int no_of_files,int *return_val);
