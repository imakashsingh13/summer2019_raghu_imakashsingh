#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
using namespace std;

long power(long p, long n)
{
	long res = 1;
	if (n == 0)
		return res;
	res = (power(p, n - 1)*p) % 1000000007;
	return res;
}

float wrapper_of_power(long a,long b)
{
	long result = 0;

	float res = result;

	if (a == 0 && b < 0)
	{
		printf("Infinite operation.\n");
		system("pause");
		exit(0);
	}
	else if (b < 0)
	{
		result = power(a, (-b));
		res = result;
		if (b % 2 == 0)
			res = 1 / (res);
		else
			res = -(1 / (res));
		return res;
	}
	
	else
	{
		result = power(a, b);
		res = result;
	}
	return res;
}

int main()
{
	int a, b;
	printf("\nEnter a and b values : ");
	scanf("%d%d", &a, &b);
	float result = wrapper_of_power(a, b);
	printf("%.2f", result);
	system("pause");
	return 0;
}