#include <stdio.h>
#include <stdlib.h>

void matrix_multiply(int matrix[2][2], int temp[2][2])
{
	int temp1 = (matrix[0][0] * temp[0][0]) + (matrix[0][1] * temp[1][0]);
	int temp2 = (matrix[0][0] * temp[0][1]) + (matrix[0][1] * temp[1][1]);
	int temp3 = (matrix[1][0] * temp[0][0]) + (matrix[1][1] * temp[1][0]);
	int temp4 = (matrix[1][0] * temp[0][1]) + (matrix[1][1] * temp[1][1]);

	matrix[0][0] = temp1;
	matrix[0][1] = temp2;
	matrix[1][0] = temp3;
	matrix[1][1] = temp4;
}

void find_power(int matrix[2][2], int num)
{
	if (num == 0 || num == 1)
		return;
	int temp[2][2] = { { 1, 1 }, { 1, 0 } };

	find_power(matrix, num / 2);
	matrix_multiply(matrix, matrix);

	if (num % 2 != 0)
		matrix_multiply(matrix, temp);
}

int fib(int num)
{
	int matrix[2][2] = { { 1, 1 }, { 1, 0 } };
	if (num == 0)
		return 0;
	find_power(matrix,num-1);
	return matrix[0][0];
}

int main()
{
	int n;
	printf("\nEnter the value of n : ");
	scanf("%d", &n);
	int result = fib(n);
	printf("%d\n", result);
	system("pause");
	return 0;
}