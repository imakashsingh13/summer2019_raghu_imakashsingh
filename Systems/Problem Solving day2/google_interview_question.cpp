/*Given N,print all the digits from 0 to largest (N-1) digit number
Eg: For N = 3 print all numbers from 0 to 99 */ 

#include <iostream>
#include <stdlib.h>
using namespace std;

void print_arr(int *arr,int n)
{
	if (n == 1)
	{
		cout << arr[0]<<"\n";
		return;
	}
	int i = 0;
	while (arr[i] == 0)
	{
		i++;
	}
	for (; i < n; i++)
	{
		cout << arr[i];
	}
	cout << "\n";
}

void generate_numbers(int limit, int *arr, int current_state)
{
	if (current_state == limit)
	{
		print_arr(arr, limit);
		return;
	}
	int opt_avail[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	for (int i = 0; i < 10; i++)
	{
		arr[current_state] = opt_avail[i];
		generate_numbers(limit, arr, current_state + 1);
	}
}

int main()
{
	int n;
	cout << "Enter N value : ";
	cin >> n;
	int *arr = (int *)calloc(n, sizeof(int));
	generate_numbers(n - 1, arr, 0);
	system("pause");
	return 0;
}