#include<stdio.h>
#include<stdlib.h>
#include <string.h>

bool is_correct_path(int no_of_row, int no_of_col, char *str, int x_pos, int y_pos)
{
	if (no_of_col < 0 || no_of_row < 0 || x_pos >= no_of_row || y_pos >= no_of_col)
		return false;
}

bool find_all_the_occurrences(char word_matrix[3][4], int no_of_row, int no_of_col, int x_pos, int y_pos, char *str)
{
	if (word_matrix[x_pos][y_pos] != str[0])
		return false;

	int row_val[] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int col_val[] = { -1, 0, 1, -1, 1, -1, 0, 1 };

	for (int i = 0; i < 8; i++)
	{
		int offset;
		int row = x_pos + row_val[i];
		int col = y_pos + col_val[i];

		int j = 1;
		for (; j < strlen(str); j++)
		{
			if (!(is_correct_path(no_of_row, no_of_col, str, row, col)))
				break;
			if (word_matrix[row][col] != str[j])
				break;
			row = row + row_val[i];
			col = col + col_val[i];
		}

		if (j == strlen(str))
		{

			printf("Word %s starting at %d %d\n",str,x_pos,y_pos);
			return true;
		}
	}
	return false;
}

int main()
{
	char word_matrix[3][4] = {"hah","hii","bdi"};
	int no_of_row=3,no_of_col=4;
	char word[] = "hii";
	for (int i = 0; i < no_of_row; i++)
	{
		for (int j = 0; j < no_of_col; j++)
		{
			find_all_the_occurrences(word_matrix, no_of_row, no_of_col, i, j, word);
		}
	}
	system("pause");
}