#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int data;
	struct node *next;
	struct node *prev;
}node;

void create_node(node **head, int val)
{
	node *temp = (node*)malloc(sizeof(node));
	temp->data = val;
	temp->next = temp->prev = NULL;
	if (*head == NULL)
		*head = temp;
	else
	{
		node *ptr = *head;
		while (ptr->next!=NULL)
		{
			ptr = ptr->next;
		}
		ptr->next = temp;
		temp->prev = ptr;
	}
}


void display(struct node *head)
{
	struct node *ptr = head;
	while (ptr != NULL)
	{
		printf("%d ", ptr->data);
		ptr = ptr->next;
	}
}

int length_of_list(struct node *head)
{
	int count = 0;
	struct node *ptr = head;
	while (ptr!=NULL)
	{
		count++;
		ptr = ptr->next;
	}
	return count;
}

node *merge(node* first, node* second)
{
	if (first == NULL)
		return second;

	if (second == NULL)
		return first;
	

	struct node *ptr1, *ptr2,*new_head;
	ptr1 = first;
	ptr2 = second;
	new_head = NULL;
	while (ptr1!=NULL && ptr2!=NULL)
	{
		if ((ptr1->data) <= (ptr2->data))
		{
			if (new_head == NULL)
			{
				new_head = ptr1;
				ptr1 = ptr1->next;
				new_head->next = NULL;
				ptr1->prev = NULL;
			}
			else
			{
				struct node *ptr = new_head;
				while (ptr->next != NULL)
				{
					ptr = ptr->next;
				}
				ptr->next = ptr1;
				ptr1 = ptr1->next;
				ptr1->prev = NULL;
				ptr->next = NULL;
			}
		}
		else
		{
			if (new_head == NULL)
			{
				new_head = ptr2;
				ptr2 = ptr2->next;
				new_head->next = NULL;
				ptr2->prev = NULL;
			}
			else
			{
				struct node *ptr = new_head;
				while (ptr->next != NULL)
				{
					ptr = ptr->next;
				}
				ptr->next = ptr2;
				ptr2 = ptr2->next;
				ptr2->prev = NULL;
				ptr->next = NULL;
			}
		}
		while (ptr1 != NULL)
		{
			struct node *ptr = new_head;
			while (ptr->next != NULL)
			{
				ptr = ptr->next;
			}
			ptr->next = ptr1;
			ptr1->prev = ptr;
		}
		while (ptr2 != NULL)
		{
			struct node *ptr = new_head;
			while (ptr->next != NULL)
			{
				ptr = ptr->next;
			}
			ptr->next = ptr2;
			ptr2->prev = ptr;
		}
	}
	return new_head;
}

node *split(node **head)
{
	node *fast = *head, *slow = *head;
	while ((fast->next!=NULL) && ((fast->next->next)!=NULL))
	{
		fast = (fast->next)->next;
		slow = slow->next;
	}
	node *ptr = slow->next;
	slow->next = NULL;
	return ptr;
}

node *mergesort(node *head1)
{
	if ((head1 == NULL) || (head1->next==NULL))
		return head1;
	node *head2 = split(&head1);
	head1 = mergesort(head1);
	head2 = mergesort(head2);
	return merge(head1,head2);
}


int main()
{
	struct node *head = NULL;
	create_node(&head, 1);
	create_node(&head, 2);
	create_node(&head, 7);
	create_node(&head, 1);
	create_node(&head, 5);
	create_node(&head, 6);

	/*while (1)
	{
		int ch;
		cout << "\n1.Insert 2.Exit";
		cin >> ch;
		if (ch == 1)
		{
			int val;
			cout << "\nEnter value to be inserted : ";
			cin >> val;
			create_node(&head, val);
		}
		else if (ch == 2)
		{
			break;
		}
	}*/

	printf("\nList before sorting: ");
	display(head);
	struct node *ptr = mergesort(head);
	printf("\nList after sorting: ");
	display(ptr);
	system("pause");
	return 0;
}