//Calculate the sum of the elements in an array using recursion

#include <iostream>
#include <stdlib.h>
using namespace std;

long sum_of_arr(int *arr, int length_of_array)
{
	long ssum = 0;
	if (length_of_array == -1)
		return ssum;
	return sum_of_arr(arr, length_of_array - 1) + arr[length_of_array];
}

int main()
{
	int no_of_elements;
	long ssum = 0;
	cout << "Enter the no of elements in the array : ";
	cin >> no_of_elements;
	int *arr = (int *)malloc(no_of_elements*sizeof(int));
	cout << "Enter the array elements : ";
	for (int i = 0; i < no_of_elements; i++)
	{
		cin >> arr[i];
	}
	ssum = sum_of_arr(arr,no_of_elements-1);
	cout << ssum<<"\n";
	system("pause");
	return 0;
}