#include <stdio.h>
#include <stdlib.h>

long fibo(long n,long *arr)
{ 
	if (arr[n] != -1)
		return arr[n];

	if (n < 0) return -1;

	if (n == 0 || n == 1)
	{
		arr[n] = n;
		return n;
	}
	arr[n] = fibo(n - 1,arr) + fibo(n - 2,arr);
	return arr[n];
}

int main()
{
	long num,res;
	long arr[100];
	for (int i = 0; i < 100; i++)
		arr[i] = -1;
	printf("\n Enter N value : ");
	scanf("%ld", &num);
	res = fibo(num,arr);
	printf("%ld\n",res);
	system("pause");
	return 0;
}