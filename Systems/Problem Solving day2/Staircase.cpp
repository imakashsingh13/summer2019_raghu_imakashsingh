// Number of ways to reach the nth step when we can climb atmost K steps
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int no_of_ways_to_climb(int step_pos, int max_leap, int count)
{
	int no_of_ways = 0;
	if (step_pos < 0)
		return 0;
	if (step_pos == 0)
		return 1;
	for (int i = 1; i <= max_leap; i++)
	{
			no_of_ways +=no_of_ways_to_climb(step_pos - i,max_leap, count+1);
	}
	return no_of_ways;
}

int main()
{
	int step_pos,max_leap;
	printf("\nEnter no of steps and max leap limit: ");
	scanf("%d%d", &step_pos, &max_leap);
	int result = no_of_ways_to_climb(step_pos,max_leap,0);
	printf("%d", result);
	system("pause");
	return 0;
}