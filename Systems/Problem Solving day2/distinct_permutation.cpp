#include <iostream>
#include <stdlib.h>
using namespace std;

void print_arr(int *arr, int n)
{
	for (int i = 0; i < n; i++)
		cout << arr[i]<<" ";
	cout << "\n";
}

void swap(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

void distinct_permutations(int *arr, int limit, int current_state)
{
	if (current_state == limit)
	{
		print_arr(arr, limit);
		return;
	}
	for (int i = current_state; i < limit; i++)
	{
		swap(arr + current_state, arr + i);
		distinct_permutations(arr, limit, current_state+1);
		swap(arr + current_state, arr + i);
	}
}

int main()
{
	int n;
	printf("\nEnter the no of elements : ");
	scanf("%d", &n);
	int *arr = (int *)calloc(n, sizeof(int));
	printf("\nEnter the array elements : ");
	for (int i = 0; i < n; i++)
		cin >> arr[i];
	distinct_permutations(arr,n,0);
	free(arr);
	system("pause");
	return 0;
}
