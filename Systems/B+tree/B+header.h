#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define pagesize 32

typedef struct nonleafpage
{
	char pagetype;
	char unused[3];
	int childpointerid[4];
	int data[3];
}nonleafpage;

typedef struct leafpage
{
	char pagetype;
	char unused[3];
	int sid[3];
	char name[3][5];
}leafpage;

typedef struct pageinfo
{
	char page_buffer[pagesize+1];
	bool used;
	time_t last_used;
	int page_id;
}pageinfo;