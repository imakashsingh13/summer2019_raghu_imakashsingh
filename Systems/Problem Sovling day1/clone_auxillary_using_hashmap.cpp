#include<iostream>
#include<stdlib.h>
#include<map>
using namespace std;

struct node
{
	int data;
	struct node* next;
	struct node* random;
};
void create_node(struct node** root, int val)
{
	struct node* newnode = (struct node*)malloc(sizeof(struct node));
	if (*root == NULL)
	{
		newnode->data = val;
		newnode->next = NULL;
		*root = newnode;
	}
	else
	{
		struct node *ptr = *root;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		newnode->data = val;
		ptr->next = newnode;
		newnode->next = NULL;
	}
}

void display(struct node *head)
{
	struct node* ptr = head;
	while (ptr != NULL)
	{
		cout << ptr->data << " ";
		cout << "Random Pointer to : " << (ptr->random)->data << " Address : " << (ptr->random) << "\n";
		ptr = ptr->next;
	}
}

void assign_random(struct node *node1, struct node *node2)
{
	node1->random = node2;
}

struct node* clone_list_using_maps(struct node *head)
{
	struct node *new_head = NULL;
	map <struct node*, struct node*> mp;
	struct node *ptr = head;
	struct node *temp = NULL;
	while (ptr != NULL)
	{
		if (new_head == NULL)
		{
			create_node(&new_head, ptr->data);
			mp.insert({ ptr, new_head});
			ptr = ptr->next;
			temp = new_head;
		}
		else
		{
			create_node(&new_head, ptr->data);
			temp = temp->next;
			mp.insert({ ptr,temp});
			ptr = ptr->next;
		}
	}
	ptr = head;
	while (ptr!=NULL)
	{
		struct node *new_address = mp[ptr->random];
		mp[ptr]->random = new_address;
		ptr = ptr->next;
	}
	return new_head;
}
int main()
{
	struct node* head = NULL;
	create_node(&head, 1);
	create_node(&head, 2);
	create_node(&head, 3);
	create_node(&head, 4);
	create_node(&head, 5);
	create_node(&head, 6);
	assign_random(head, head->next->next); // 1 -> 3
	assign_random(head->next, head->next->next->next->next->next); // 2 -> 6
	assign_random(head->next->next, head->next); // 3 -> 2
	assign_random(head->next->next->next, head); // 4 -> 1
	assign_random(head->next->next->next->next, head->next->next->next); // 5 -> 4
	assign_random(head->next->next->next->next->next, head->next->next->next->next); // 6 -> 5
	cout << "\n Original list : \n";
	display(head);
	struct node *new_head = clone_list_using_maps(head);
	cout << "\n Cloned list : \n";
	display(new_head);
	system("pause");
	return 0;
}