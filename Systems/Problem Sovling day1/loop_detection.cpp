#include<iostream>
#include<stdlib.h>
using namespace std;

struct node
{
	int data;
	struct node* next;
};
void create_node(struct node** root, int val)
{
	struct node* newnode = (struct node*)malloc(sizeof(struct node));
	if (*root == NULL)
	{
		newnode->data = val;
		newnode->next = NULL;
		*root = newnode;
	}
	else
	{
		struct node *ptr = *root;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		newnode->data = val;
		ptr->next = newnode;
		newnode->next = NULL;
	}
}

void display(struct node *head)
{
	struct node* ptr = head;
	while (ptr->next != NULL)
	{
		cout << ptr->data << " ";
		ptr = ptr->next;
	}
}

bool loop_detection_in_ll(struct node *head)
{
	struct node *fast=head, *slow = head;
	while (fast != NULL && slow != NULL)
	{
		slow = slow->next;
		if ((fast->next) == NULL) return false;
		fast = (fast->next)->next;
		if (slow == fast) return true;
	}
	return false;
}

int main()
{
	struct node* head = NULL;
	create_node(&head, 1);
	create_node(&head, 2);
	create_node(&head, 3);
	create_node(&head, 4);
	create_node(&head, 5);
	//(((head->next)->next)->next)->next = head->next;  // 5 -> 2
	bool result = loop_detection_in_ll(head);
	if (result==true)
		cout << "There is a loop in the linked list.";
	else
		cout << "There is no loop in the linked list.";
	system("pause");
	return 0;
}