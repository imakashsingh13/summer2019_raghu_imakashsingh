#include<iostream>
#include<stdlib.h>
using namespace std;

int* spiral_matrix(int **array, int no_of_rows, int no_of_cols)
{
	int *result_array = (int *)calloc((no_of_rows*no_of_rows), sizeof(int));
	int row_pos = 0;
	int col_pos = 0;
	int ending_row_no = no_of_rows - 1;
	int ending_col_no = no_of_cols - 1;
	int starting_row_no = 0;
	int starting_col_no = 0;
	int count_var = 0;
	while (count_var < (no_of_rows*no_of_cols))
	{
		if (row_pos == starting_row_no && col_pos == starting_col_no)
		{
			while (col_pos <= ending_col_no)
			{
				result_array[count_var++] = array[row_pos][col_pos++];
			}
			starting_row_no++;
			row_pos = starting_row_no;
			col_pos = ending_col_no;
		}
		else if (row_pos == starting_row_no && col_pos == ending_col_no)
		{
			while (row_pos <= ending_row_no)
			{
				result_array[count_var++] = array[row_pos++][col_pos];
			}
			ending_col_no--;
			row_pos = ending_row_no;
			col_pos = ending_col_no;
		}
		else if (row_pos == ending_row_no && col_pos == ending_col_no)
		{
			while (col_pos >= starting_col_no)
			{
				result_array[count_var++] = array[row_pos][col_pos--];
			}
			ending_row_no--;
			row_pos = ending_row_no;
			col_pos = starting_col_no;
		}
		else if (row_pos == ending_row_no && col_pos == starting_col_no)
		{
			while (row_pos >= starting_row_no)
			{
				result_array[count_var++] = array[row_pos--][col_pos];
			}
			starting_col_no++;
			row_pos = starting_row_no;
			col_pos = starting_col_no;
		}
	}
	return result_array;
}

int main()
{
	int r, c;
	cout << "Enter the no. of rows and columns: ";
	cin >> r >> c;
	int **array = (int **)malloc(r * sizeof(int *));
	for (int i = 0; i < r; i++)
		array[i] = (int *)malloc(c * sizeof(int));
	cout << "\nEnter the elements of the matrix: ";
	for (int i = 0; i<r; i++)
		for (int j = 0; j<c; j++)
			cin >> array[i][j];
	int *res = spiral_matrix(array, r, c);
	for (int i = 0; i<(r*c); i++)
		cout << res[i] << " ";
	system("pause");
	return 0;
}