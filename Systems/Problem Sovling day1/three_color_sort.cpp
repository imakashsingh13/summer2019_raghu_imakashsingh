#include<iostream>
#include<stdlib.h>
using namespace std;
void sswap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}
void three_color_sort(int *array, int len_of_array)
{
	int low = 0;
	int mid = 0;
	int high = len_of_array - 1;
	while (mid <= high)
	{
		if (array[mid] == 0)
		{
			sswap((array + low), (array + mid));
			low++;
			mid++;
		}
		else if (array[mid] == 1)
		{
			mid++;
		}
		else
		{
			sswap((array + mid), (array + high));
			high--;
		}
	}
}
int main()
{
	int n;
	cout << "\nEnter size of the array : ";
	cin >> n;
	int *array = (int *)calloc(n, sizeof(int));
	cout << "\nEnter elements in the array : ";
	for (int i = 0; i < n; i++)
		cin >> array[i];
	three_color_sort(array, n);
	cout << "Array after sorting: ";
	for (int i = 0; i < n; i++)
		cout << array[i] << " ";
	system("pause");
	return 0;
}