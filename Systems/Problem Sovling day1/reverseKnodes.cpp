#include<iostream>
#include<stdlib.h>
#include<stack>
using namespace std;

struct node
{
	int data;
	struct node* next;
};
void create_node(struct node** root, int val)
{
	struct node* newnode = (struct node*)malloc(sizeof(struct node));
	if (*root == NULL)
	{
		newnode->data = val;
		newnode->next = NULL;
		*root = newnode;
	}
	else
	{
		struct node *ptr = *root;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		newnode->data = val;
		ptr->next = newnode;
		newnode->next = NULL;
	}
}

void display(struct node *head)
{
	struct node* ptr = head;
	while (ptr != NULL)
	{
		cout << ptr->data << " ";
		ptr = ptr->next;
	}
}

int len_of_list(struct node *head)
{
	struct node *ptr = head;
	int ln = 0;
	while (ptr != NULL)
	{
		ptr = ptr->next;
		ln++;
	}
	return ln;
}
struct node* reverse(struct node** head)
{
	stack <struct node*> st;
	struct node *ptr = *head;
	while (ptr != NULL)
	{
		st.push(ptr);
		ptr = ptr->next;
	}
	struct node* new_head = NULL;
	struct node *temp = NULL;
	while (!st.empty())
	{
		temp = st.top();
		if (new_head == NULL)
		{
			new_head = temp;
			ptr = new_head;
		}
		else
		{
			ptr->next = temp;
			ptr = temp;
		}
		st.pop();
	}
	ptr->next = NULL;
	return new_head;
}
struct node* reverse_k_nodes(struct node *head, int k)
{
	struct node *new_hd = NULL;
	int n = len_of_list(head);
	if (head == NULL) return NULL;
	else if (n < k) return head;
	else if (n >= k)
	{
		int cn = 1;
		struct node *ptr = head;
		int t = n / k;
		struct node *pre = NULL;
		struct node *next_hd = NULL;
		struct node *cur_hd = head;
		struct node *tmp_hd = NULL;
		while (t--)
		{
			while (cn < k)
			{
				ptr = ptr->next;
				cn++;
			}
			next_hd = ptr->next;
			ptr->next = NULL;
			if (pre == NULL)
			{
				tmp_hd = reverse(&cur_hd);
				new_hd = tmp_hd;
			}
			else
			{
				pre->next = NULL;
				tmp_hd = reverse(&cur_hd);
			}
			if (pre != NULL)
			{
				pre->next = tmp_hd;
			}
			struct node *temp = tmp_hd;
			while (temp->next!=NULL)
			{
				temp = temp->next;
			}
			pre = temp;
			pre->next = next_hd;
			ptr = next_hd;
			cur_hd = next_hd;
			cn = 1;
		}

	}
	return new_hd;
}
int main()
{
	struct node *head = NULL;
	while (1)
	{
		int ch;
		cout << "\n1.Insert 2.Exit";
		cin >> ch;
		if (ch == 1)
		{
			int val;
			cout << "\nEnter value to be inserted : ";
			cin >> val;
			create_node(&head, val);
		}
		else if (ch == 2)
		{
			break;
		}
	}
	cout << "\n List before reverse: ";
	display(head);
	int k;
	cout << "\nEnter k value: ";
	cin >> k;
	struct node *new_head = reverse_k_nodes(head, k);
	cout << "\n List after reverse: ";
	display(new_head);
	system("pause");
	return 0;
}
