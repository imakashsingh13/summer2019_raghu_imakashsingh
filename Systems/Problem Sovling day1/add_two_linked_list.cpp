#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct node
{
	int data;
	struct node* next;
};

void create_node(struct node** root, int val)
{
	struct node* newnode = (struct node*)malloc(sizeof(struct node));
	if (*root == NULL)
	{
		newnode->data = val;
		newnode->next = NULL;
		*root = newnode;
	}
	else
	{
		struct node *ptr = *root;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		newnode->data = val;
		ptr->next = newnode;
		newnode->next = NULL;
	}
}

void swap(struct node** head1, struct node** head2)
{
	struct node* temp = *head1;
	*head1 = *head2;
	*head2 = temp;
}

void insert_begin(struct node** head, int val)
{
	struct node *newnode = (struct node *)malloc(sizeof(struct node));
	newnode->data = val;
	newnode->next = *head;
	*head = newnode;
}

void display(struct node *head)
{
	struct node* ptr = head;
	while (ptr != NULL)
	{
		printf("%d ", ptr->data);
		ptr = ptr->next;
	}
}

int len_of_list(struct node *head)
{
	struct node *ptr = head;
	int len = 0;
	while (ptr != NULL)
	{
		ptr = ptr->next;
		len++;
	}
	return len;
}

struct node* create_list(struct node **head)
{
	while (1)
	{
		int ch;
		printf("\n1.Insert 2.Exit");
		scanf("%d", &ch);
		if (ch == 1)
		{
			int val;
			printf("\nEnter value to be inserted : ");
			scanf("%d", &val);
			create_node(head, val);
		}
		else if (ch == 2)
		{
			break;
		}
	}
	return *head;
}

struct node* add_same_size(struct node* head1,struct node* head2,int *carry)
{
	int ssum = 0;

	if (head1 == NULL)
		return NULL;

	struct node *result = (struct node*)malloc(sizeof(struct node));
	result->next = add_same_size(head1->next, head2->next, carry);
	ssum = ((head1->data) + (head2->data)) + (*carry);
	*carry = ssum / 10;
	ssum = ssum % 10;
	result->data = ssum;
	return result;
}

void propagate_carry(struct node* head, struct node* ptr, int *carry, struct node **result)
{
	int ssum = 0;
	if (head != ptr)
	{
		propagate_carry(head->next, ptr, carry, result);

		ssum = (head->data) + (*carry);
		*carry = ssum / 10;
		ssum = ssum % 10;
		insert_begin(result, ssum);
	}
}

void add_two_lists(struct node *head1, struct node *head2, struct node **result)
{
	if (head1 == NULL)
	{
		*result = head1;
		return;
	}
	if (head2 == NULL)
	{
		*result = head2;
	}
	int len1 = len_of_list(head1);
	int len2 = len_of_list(head2);
	int difference = abs(len1 - len2);
	int carry = 0;
	if (difference == 0)
	{
		*result = add_same_size(head1, head2, &carry);
		return;
	}
	else
	{
		if (len1 < len2)
			swap(&head1, &head2);

		struct node *ptr = head1;
		while (difference > 0)
		{
			ptr = ptr->next;
			difference--;
		}
		*result = add_same_size(ptr, head2, &carry);

		propagate_carry(head1,ptr,&carry,result);
	}
	if (carry != 0)
		insert_begin(result, carry);
}

int main()
{
	struct node *head1 = NULL;
	struct node *head2 = NULL;
	struct node *result = NULL;
	printf("\nEnter the elements of the first list : ");
	head1 = create_list(&head1);

	printf("\nEnter the elements of the second list : ");
	head2 = create_list(&head2);

	printf("\nElements of first list\n");
	display(head1);

	printf("\nElements of second list\n");
	display(head2);

	add_two_lists(head1, head2, &result);
	printf("\nResultant sum of two lists : ");
	display(result);

	free(head1);
	free(head2);
	system("pause");
	return 0;
}