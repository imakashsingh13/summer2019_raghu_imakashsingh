#include<iostream>
#include<stdlib.h>
using namespace std;

void matrix_transpose(int **matrix, int **transpose, int r, int c)
{
	for (int i = 0; i<r; i++)
	{
		for (int j = 0; j<c; j++)
		{
			transpose[j][i] = matrix[i][j];
		}
	}
}
int main()
{
	int r, c;
	cout << "Enter the no. of rows and columns: ";
	cin >> r >> c;
	int **matrix = (int **)malloc(r * sizeof(int *));
	for (int i = 0; i < r; i++)
		matrix[i] = (int *)malloc(c * sizeof(int));
	int **transpose_matrix = (int **)malloc(c*sizeof(int *));
	for (int i = 0; i < c; i++)
		transpose_matrix[i] = (int *)malloc(r*sizeof(int));
	
	cout << "\nEnter the elements of the matrix: ";
	for (int i = 0; i<r; i++)
		for (int j = 0; j<c; j++)
			cin >> matrix[i][j];

	matrix_transpose(matrix, transpose_matrix, r, c);
	
	cout << "\n Input Matrix: \n";
	for (int i = 0; i<r; i++)
	{
		for (int j = 0; j<c; j++)
		{
			cout << matrix[i][j] << " ";
		}
		cout << "\n";
	}

	cout << "\n Transposed Matrix : \n";
	for (int i = 0; i<c; i++)
	{
		for (int j = 0; j<r; j++)
		{
			cout << transpose_matrix[i][j] << " ";
		}
		cout << "\n";
	}

	system("pause");
	return 0;
}