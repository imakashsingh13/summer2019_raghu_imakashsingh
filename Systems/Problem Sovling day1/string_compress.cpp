#include<iostream>
#include<stdlib.h>
#include<string.h>
using namespace std;

char* string_compress(char *str, int len_of_str)
{
	int count_var = 0, prev = 0;
	int itr = 0;
	while (str[itr] != '\0' && itr<len_of_str)
	{
		while (str[itr] == str[itr + 1])
		{
			count_var++;
			itr++;
		}
		if (count_var <= 1)
		{
			if (count_var == 0)
			{
				str[prev] = str[itr];
				prev++;
			}
			else
			{
				str[prev++] = str[itr];
				str[prev++] = (count_var + 1) + '0';
				count_var = 0;
			}
		}
		else
		{
			if (count_var >= 9)
			{
				count_var++;
				int temp[10] = { 0 };
				int temp_len = 0;
				str[prev++] = str[itr];
				while (count_var>0)
				{
					temp[temp_len++] = count_var % 10;
					count_var /= 10;
				}
				temp_len--;
				while (temp_len >= 0)
				{
					str[prev++] = temp[temp_len] + '0';
					temp_len--;
				}
				count_var = 0;
			}
			else
			{
				str[prev++] = str[itr];
				str[prev++] = (count_var + 1) + '0';
				count_var = 0;
			}
		}
		itr++;
	}
	str[prev] = '\0';
	return str;
}
int main()
{
	char str[200];
	cout << "\nEnter the string: ";
	cin >> str;
	char *result_str = string_compress(str, strlen(str));
	cout << result_str<<"\n";
	system("pause");
	return 0;
}