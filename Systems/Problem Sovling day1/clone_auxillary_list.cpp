#include<iostream>
#include<stdlib.h>
using namespace std;

struct node
{
	int data;
	struct node* next;
	struct node* random;
};
void create_node(struct node** root, int val)
{
	struct node* newnode = (struct node*)malloc(sizeof(struct node));
	if (*root == NULL)
	{
		newnode->data = val;
		newnode->next = NULL;
		*root = newnode;
	}
	else
	{
		struct node *ptr = *root;
		while (ptr->next != NULL)
		{
			ptr = ptr->next;
		}
		newnode->data = val;
		ptr->next = newnode;
		newnode->next = NULL;
	}
}

void display(struct node *head)
{
	struct node* ptr = head;
	while (ptr != NULL)
	{
		cout << ptr->data << " ";
		//cout << "Random Pointer to : " << (ptr->random)->data << "Address : "<<(ptr->random)<< "\n";
		ptr = ptr->next;
	}
}

void assign_random(struct node *node1, struct node *node2)
{
	node1->random = node2;
}

struct node* clone_list(struct node*head)
{
	if (head == NULL) return NULL;
	struct node* ptr, *preptr, *temp = NULL;
	ptr = head;
	while (ptr != NULL)
	{
		struct node *newnode = (struct node*)malloc(sizeof(struct node));
		temp = ptr->next;
		newnode->data = ptr->data;
		ptr->next = newnode;
		newnode->next = temp;
		ptr = temp;
	}
	ptr = head;
	while (ptr != NULL)
	{
		temp = ptr->next;
		temp->random = (ptr->random)->next;
		ptr = temp->next;
	}
	ptr = head;
	head = ptr->next;
	ptr->next = NULL;
	ptr = head;
	while (ptr != NULL)
	{
		temp = ptr->next;
		if (temp == NULL)
		{
			ptr->next = NULL;
		}
		else
		{
			ptr->next = temp->next;
			temp->next = NULL;
		}
		ptr = ptr->next;
	}
	return head;
}
int main()
{
	struct node* head = NULL;
	create_node(&head, 1);
	create_node(&head, 2);
	create_node(&head, 3);
	create_node(&head, 4);
	create_node(&head, 5);
	create_node(&head, 6);
	assign_random(head, head->next->next); // 1 -> 3
	assign_random(head->next, head->next->next->next->next->next); // 2 -> 6
	assign_random(head->next->next, head->next); // 3 -> 2
	assign_random(head->next->next->next, head); // 4 -> 1
	assign_random(head->next->next->next->next, head->next->next->next); // 5 -> 4
	assign_random(head->next->next->next->next->next, head->next->next->next->next); // 6 -> 5
	cout << "\n Original list : \n";
	display(head);
	struct node *new_head = clone_list(head);
	cout << "\n Cloned list : \n";
	display(new_head);
	system("pause");
	return 0;
}