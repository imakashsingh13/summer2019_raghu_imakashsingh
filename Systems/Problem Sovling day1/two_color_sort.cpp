#include<iostream>
#include<stdlib.h>
using namespace std;

void sort_using_count_var(int *array,int len_of_array)
{
	int ssum = 0;
	for(int i = 0;i < len_of_array; i++)
	{
		ssum += array[i];
	}
	int i = 0;
	for(;i < ssum;i++)
		array[i] = 0;
	for(;i < len_of_array;i++)
		array[i] = 1;
}
void bubble_sort(int *array, int len_of_array)
{
	for (int i = 0; i < len_of_array-1; i++)
	{
		for (int j = 0; j < len_of_array-i-1; j++)
		{
			if (array[j] > array[j + 1])
			{
				int temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
}
void sort_using_two_flags(int *array, int len_of_array)
{
	int starting_flag = 0;
	int ending_flag = len_of_array-1;
	while (starting_flag < ending_flag)
	{
		while (array[starting_flag] == 0 && starting_flag < ending_flag)
			starting_flag++;
		while (array[ending_flag] == 1 && starting_flag < ending_flag)
			ending_flag--;
		if (starting_flag < ending_flag)
		{
			array[starting_flag] = 0;
			array[ending_flag] = 1;
			starting_flag++;
			ending_flag--;
		}

	}
}
int main()
{
	int n;
	cout << "\nEnter size of the array : ";
	cin >> n;
	int *array = (int *)calloc(n, sizeof(int));
	cout << "\nEnter elements in the array : ";
	for (int i = 0; i < n; i++)
		cin >> array[i];
	sort_using_count_var(array, n);
	cout << "Array after sorting: ";
	for (int i = 0; i < n; i++)
		cout << array[i] << " ";
	system("pause");
	return 0;
}
